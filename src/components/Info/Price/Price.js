import React from 'react';

const Price = props => {
  return (
      <div className="price">
        Current Price: <span>{props.price} som</span>
      </div>
  );
};

export default Price;