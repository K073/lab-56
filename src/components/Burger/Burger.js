import React from 'react';
import Ingredient from "./Ingredient/Ingredient";

const Burger = props => {
  const ingredients = [];
  for (let key in props.ingredients){
    for (let i = 0; i < props.ingredients[key]; i++){
      ingredients.push(<Ingredient key={key + i} type={key}/>)
    }
  }
  return (
      <div className="Burger">
        <div className="BreadTop">
          <div className="Seeds1" />
          <div className="Seeds2" />
        </div>
        {ingredients}
        <div className="BreadBottom" />
      </div>
  );
};

export default Burger;